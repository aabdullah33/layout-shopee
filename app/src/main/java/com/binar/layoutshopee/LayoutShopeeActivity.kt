package com.binar.layoutshopee

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.binar.layoutshopee.databinding.ActivityLayoutShopeeBinding
import com.google.android.material.bottomnavigation.BottomNavigationView


class LayoutShopeeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLayoutShopeeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLayoutShopeeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        getSupportActionBar()?.hide();

        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_layout)

        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_recommendation,
                R.id.navigation_feed,
                R.id.navigation_live,
                R.id.navigation_notifications
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }
}