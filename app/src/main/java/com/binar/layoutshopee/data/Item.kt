package com.binar.layoutshopee.data

data class Item(
        var name: String = "",
        var logo: Int = 0
)