package com.binar.layoutshopee.data

class Grid{
    var desc: String = ""
    var logo: Int = 0

    constructor(desc: String, logo : Int){
        this.desc = desc
        this.logo = logo
    }
}