package com.binar.layoutshopee.data

import com.binar.layoutshopee.R
import java.util.*


object ItemData {
    private val itemPrice = arrayOf("Rp62.500",
    "Rp32.500",
    "Rp50.000")

    private val itemPic = intArrayOf(
        R.drawable.minyak,
        R.drawable.jaket,
        R.drawable.shampoo
    )



    val listData: ArrayList<Item>
        get() {
            val list = arrayListOf<Item>()
            for (position in itemPrice.indices) {
                val item = Item()
                item.name = itemPrice[position]
                item.logo = itemPic[position]
                list.add(item)
            }
            return list
        }
}