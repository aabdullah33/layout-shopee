package com.binar.layoutshopee.ui.recommendation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.binar.layoutshopee.R
import com.binar.layoutshopee.data.Item
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import java.util.ArrayList

class FlashAdapter(private val flashItems: ArrayList<Item>) : RecyclerView.Adapter<FlashAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_flash, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val flashItems = flashItems[position]
        holder.tvName.text = flashItems.name
        Glide.with(holder.itemView.context)
            .load(flashItems.logo)
            .into(holder.imgLogo)
    }

    override fun getItemCount() = flashItems.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvName: TextView = view.findViewById(R.id.textView)
        var imgLogo: ImageView = view.findViewById(R.id.imgVItem)
    }
}
