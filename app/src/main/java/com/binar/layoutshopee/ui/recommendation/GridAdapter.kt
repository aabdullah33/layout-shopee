package com.binar.layoutshopee.ui.recommendation

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.binar.layoutshopee.R
import com.binar.layoutshopee.data.Grid

class GridAdapter(var context: Context?, var arrayList: ArrayList<Grid>) : BaseAdapter() {
    override fun getItem(position: Int): Any {
        return arrayList.get(position)
    }

    override fun getItemId(positon: Int): Long {
        return positon.toLong()
    }

    override fun getCount(): Int {
        return arrayList.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view:View = View.inflate(context, R.layout.item_grid, null)

        var logo:ImageView = view.findViewById(R.id.imgVGrid)
        var desc:TextView = view.findViewById(R.id.tvGrid)

        var gridItem:Grid = arrayList.get(position)

        logo.setImageResource(gridItem.logo)
        desc.text = gridItem.desc

        return view
    }
}