package com.binar.layoutshopee.ui.recommendation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.binar.layoutshopee.R
import com.binar.layoutshopee.data.Grid
import com.binar.layoutshopee.data.Item
import com.binar.layoutshopee.data.ItemData
import com.binar.layoutshopee.databinding.FragmentRecommendationBinding
import java.util.ArrayList

class RecommendationFragment : Fragment() {

    private var _binding: FragmentRecommendationBinding? = null
    private val binding get() = _binding!!
    private val list = ArrayList<Item>()
    private var gridView: GridView? = null
    private var arrayList: ArrayList<Grid>? = null
    private var gridAdapter: GridAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentRecommendationBinding.inflate(inflater, container, false)
        val root: View = binding.root

        gridView = binding.gridView
        arrayList = ArrayList()
        arrayList = setDataGrid()
        gridAdapter = GridAdapter(context, arrayList!!)
        gridView?.adapter = gridAdapter

        showRecyclerList()

        return root
    }

    private fun showRecyclerList() {
        list.addAll(ItemData.listData)
        binding.rvFlash.layoutManager =
            LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)
        val ItemFlashAdapter = FlashAdapter(list)
        binding.rvFlash.adapter = ItemFlashAdapter
    }

    private fun setDataGrid(): ArrayList<Grid>{
        var arrayList:ArrayList<Grid> = ArrayList()

        arrayList.add(Grid("PeduliLindungi", R.drawable.ic_baseline_health_and_safety_24))
        arrayList.add(Grid("Pulsa", R.drawable.ic_baseline_phone_android_24))
        arrayList.add(Grid("Games", R.drawable.ic_baseline_videogame_asset_24))
        arrayList.add(Grid("Mall", R.drawable.ic_baseline_shopping_bag_24))
        arrayList.add(Grid("Supermarket", R.drawable.ic_baseline_shopping_cart_24))
        arrayList.add(Grid("Food", R.drawable.ic_baseline_food_bank_24))
        arrayList.add(Grid("Flash Sale", R.drawable.ic_baseline_flash_on_24))
        arrayList.add(Grid("Ticket", R.drawable.ic_baseline_attractions_24))

        return arrayList
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}